#! /usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib import admin
from django.conf.urls import include, url

urlpatterns = [
    url(r'^summernote/', include('django_summernote.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'', include('app.urls')),
]