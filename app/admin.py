from django.contrib import admin
from .models import Solution
from .models import MenuItem
from django import forms
from django_summernote.widgets import SummernoteWidget    

admin.site.register(Solution)
admin.site.register(MenuItem)