from django import forms
from .models import Solution
from .models import MenuItem
from .models import SlideShowItem
from .models import Article
from .models import Page
from django_summernote.widgets import SummernoteInplaceWidget, SummernoteWidget

class SolutionForm(forms.ModelForm):
    desc_short = forms.CharField(widget=SummernoteWidget(attrs={'width': '100%', 'height': '200px'}))
    introduction_text = forms.CharField(widget=SummernoteWidget(attrs={'width': '100%', 'height': '200px'}))
    full_text = forms.CharField(widget=SummernoteWidget(attrs={'width': '100%', 'height': '400px'}))
    visible = forms.BooleanField(widget=forms.CheckboxInput(attrs={'class':'form-control'}), required=False)

    class Meta:
        model = Solution
        fields = ('status', 'visible', 'order', 'name', 'title', 'grid_title', 'grid_image', 'url_control', 'desc_short', 
                  'desc_short', 'desc_short_image', 'introduction_title', 'introduction_text', 'full_text')

class MenuForm(forms.ModelForm):
    class Meta:
        model = MenuItem
        fields = ('order', 'name', 'parent', 'title', 'url', 'is_parent')

class ArticleForm(forms.ModelForm):
    intro = forms.CharField(widget=SummernoteWidget(attrs={'width': '100%', 'height': '200px'}))
    text = forms.CharField(widget=SummernoteWidget(attrs={'width': '100%', 'height': '400px'}))
    title = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}))
    published = forms.BooleanField(widget=forms.CheckboxInput(attrs={'class':'form-control'}), required=False)
    class Meta:
        model = Article
        fields = ('published','title','intro','text',)

class PageForm(forms.ModelForm):
    visible = forms.BooleanField(widget=forms.CheckboxInput(attrs={'class':'form-control'}), required=False)
    name = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}))   
    url = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}))
    text = forms.CharField(widget=SummernoteWidget(attrs={'width': '100%', 'height': '400px'}))

    class Meta:
        model = Page
        fields = ('visible','name', 'url','text')