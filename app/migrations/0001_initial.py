# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-07 02:39
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('published', models.BooleanField(default=True)),
                ('title', models.CharField(max_length=75)),
                ('intro', models.CharField(max_length=2000)),
                ('text', models.TextField()),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Feedback',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('author', models.CharField(max_length=200)),
                ('email', models.CharField(max_length=100)),
                ('topic', models.CharField(max_length=200)),
                ('text', models.CharField(max_length=2000)),
                ('ip', models.CharField(max_length=20)),
                ('created_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('usr', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='MenuItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('order', models.IntegerField(default=0)),
                ('name', models.CharField(max_length=20)),
                ('parent', models.CharField(blank=True, max_length=20)),
                ('title', models.CharField(max_length=200)),
                ('url', models.CharField(blank=True, max_length=200)),
                ('is_parent', models.BooleanField()),
            ],
        ),
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('visible', models.BooleanField(default=True)),
                ('name', models.CharField(max_length=75)),
                ('url', models.CharField(max_length=75)),
                ('text', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Settings',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('site_active', models.BooleanField(default=True)),
                ('site_message', models.BooleanField(default=True)),
                ('address', models.CharField(max_length=100)),
                ('phone', models.CharField(max_length=100)),
                ('email', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='SlideShowItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('order', models.IntegerField(default=0)),
                ('background', models.CharField(default=b'default_slideshow.jpg', max_length=100)),
                ('title', models.CharField(default=b'\xd0\x97\xd0\xb0\xd0\xb3\xd0\xbe\xd0\xbb\xd0\xbe\xd0\xb2\xd0\xbe\xd0\xba', max_length=100)),
                ('description', models.CharField(default=b'\xd0\x9e\xd0\xbf\xd0\xb8\xd1\x81\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5', max_length=1000)),
                ('button_text', models.CharField(default=b'\xd0\x9f\xd0\xb5\xd1\x80\xd0\xb5\xd0\xb9\xd1\x82\xd0\xb8', max_length=50)),
                ('button_url', models.CharField(default=b'#', max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Solution',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('order', models.IntegerField(default=0)),
                ('status', models.IntegerField(default=0)),
                ('visible', models.BooleanField()),
                ('name', models.CharField(max_length=100)),
                ('title', models.CharField(max_length=100)),
                ('grid_title', models.CharField(max_length=200)),
                ('grid_image', models.CharField(max_length=100)),
                ('url_control', models.CharField(max_length=100)),
                ('desc_short', models.CharField(max_length=1000)),
                ('desc_short_image', models.CharField(max_length=100)),
                ('introduction_title', models.TextField(max_length=200)),
                ('introduction_text', models.TextField(max_length=1000)),
                ('full_text', models.CharField(max_length=20000)),
                ('created_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('activation', models.CharField(max_length=20)),
                ('patronymic', models.CharField(max_length=20)),
                ('company', models.CharField(max_length=20)),
                ('address', models.CharField(max_length=100)),
                ('phone', models.CharField(max_length=20)),
                ('solutions', models.CharField(max_length=500)),
                ('avatar', models.CharField(default=b'unknown.jpg', max_length=100)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
