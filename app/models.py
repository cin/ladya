#! /usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.utils import timezone

class UserProfile(models.Model):
    user        = models.OneToOneField(User, on_delete=models.CASCADE)
    activation  = models.CharField(max_length=20)
    patronymic  = models.CharField(max_length=20)
    company     = models.CharField(max_length=20)
    address     = models.CharField(max_length=100)
    phone       = models.CharField(max_length=20)
    solutions   = models.CharField(max_length=500)
    avatar      = models.CharField(max_length=100, default="unknown.jpg")

class Solution(models.Model):
    author              = models.ForeignKey('auth.User')            # Автор
    order               = models.IntegerField(default=0)            # Порядок решения
    status              = models.IntegerField(default=0)            # Статус решения
    visible             = models.BooleanField()                     # Видно ли решение
    name                = models.CharField(max_length=100)          # Имя решения
    title               = models.CharField(max_length=100)          # Название решения
    grid_title          = models.CharField(max_length=200)          # Описание для сетки решений
    grid_image          = models.CharField(max_length=100)          # Адрес картинки для сетки решений (700x467)
    url_control         = models.CharField(max_length=100)          # Адрес панели управления для решения
    desc_short          = models.CharField(max_length=1000)         # Краткое описание
    desc_short_image    = models.CharField(max_length=100)          # Адрес картинки для краткого описания, круглое (140x140)
    introduction_title  = models.TextField(max_length=200)          # Заголовок статьи
    introduction_text   = models.TextField(max_length=1000)         # Вступительная часть статьи
    full_text           = models.CharField(max_length=20000)        # Статья целиком
    created_date        = models.DateTimeField(default=timezone.now)# Время создания

    def __str__(self):
        return self.title

class MenuItem(models.Model):
    order       = models.IntegerField(default=0)
    name        = models.CharField(max_length=20)
    parent      = models.CharField(max_length=20, blank=True)
    title       = models.CharField(max_length=200)
    url         = models.CharField(max_length=200, blank=True)
    is_parent   = models.BooleanField()

    def __str__(self):
        return self.title

class SlideShowItem(models.Model):
    order       = models.IntegerField(default=0)                                    # порядок
    background  = models.CharField(max_length=100,  default='default_slideshow.jpg') # адрес изображения
    title       = models.CharField(max_length=100,  default='Заголовок')             # заголовок
    description = models.CharField(max_length=1000, default='Описание')             # описание
    button_text = models.CharField(max_length=50,   default='Перейти')                # текст кнопки
    button_url  = models.CharField(max_length=200,  default='#')                     # адрес кнопки
    
    def __str__(self):
        return self.title

class Article(models.Model):
    author      = models.ForeignKey('auth.User')            # Автор
    created_date= models.DateTimeField(default=timezone.now)# Время создания
    published   = models.BooleanField(default=True)         # Признак публикации
    title       = models.CharField(max_length=75)           # Заголовок
    intro       = models.CharField(max_length=2000)         # Вступительная часть статьи   
    text        = models.TextField()                        # Полный текст статьи
    
    def __str__(self):
        return self.title

class Page(models.Model):
    visible     = models.BooleanField(default=True)         # Признак публикации
    name        = models.CharField(max_length=75)           # Имя
    url         = models.CharField(max_length=75)           # Адрес
    text        = models.TextField()                        # Полный текст
    
    def __str__(self):
        return self.url

class Settings(models.Model):
    site_active  = models.BooleanField(default=True)        # открыт ли сайт
    site_message = models.CharField(max_length=200)         # сообщение при закрытом сайте
    address      = models.CharField(max_length=100)         # адрес
    phone        = models.CharField(max_length=100)         # телефон
    email        = models.CharField(max_length=100)         # email

class Feedback(models.Model):
    usr         = models.ForeignKey('auth.User')            # Автор
    author      = models.CharField(max_length=200)          # Имя Фамилия
    email       = models.CharField(max_length=100)          # EMail
    topic       = models.CharField(max_length=200)          # Тема
    text        = models.CharField(max_length=2000)         # Текст
    ip          = models.CharField(max_length=20)           # IP-адрес
    created_date= models.DateTimeField(default=timezone.now)# Время отправки    

class ProductCategory(models.Model):
    name        = models.CharField(max_length=70)           # имя категории
    title       = models.CharField(max_length=100)          # заголовок категории

class ProductSubCategory(models.Model):
    category    = models.ForeignKey(ProductCategory)        # родительская категория
    name        = models.CharField(max_length=70)           # имя подкатегории
    title       = models.CharField(max_length=100)          # заголовок подкатегории

class Product(models.Model):
    subcategory = models.ForeignKey(ProductCategory)        # подкатегория
    title       = models.CharField(max_length=200)          # название
    price       = models.IntegerField(default=0)            # стоимость
    desc        = models.CharField(max_length=2000)         # описание
    images      = models.CharField(max_length=500)          # изображения товара разделенные ';'
    count       = models.IntegerField(default=999999)       # остаток на складе
    product_type= models.CharField(max_length=15)           # тип

class CartElement(models.Model):
    usr         = models.ForeignKey('auth.User')            # пользователь
    product     = models.ForeignKey(Product)                # товар
    count       = models.IntegerField(default=1)            # количество

class Cart(models.Model):
    usr         = models.ForeignKey('auth.User')            # пользователь
    element     = models.ManyToManyField(CartElement)       # список покупок

class Order(models.Model):
    usr         = models.ForeignKey('auth.User')            # пользователь
    status      = models.IntegerField(default=0)            # статус (0: неподтверждён, 1:неоплачен, 2:оплачен)