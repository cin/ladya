import random
import os
from django import template
from app.models import SlideShowItem
from app.models import MenuItem
from app.models import Solution
from app.models import Article
from app.models import Feedback
from app.models import Settings

register = template.Library()

@register.simple_tag
def random_int(a, b=None):
    if b is None:
        a, b = 0, a
    return random.randint(a, b)

@register.filter
def thumb(value):
    filename, ext = os.path.splitext(value)
    return filename+"_thumb"+ext

register.filter('thumb', thumb)
 
@register.inclusion_tag('app/main_menu.html')
def render_menu(r):
    menu_items = MenuItem.objects.filter(parent__exact='').order_by('order')
    menu_items_names = menu_items.filter(parent__exact='').values('name').order_by('order')
    sub_menu_items = MenuItem.objects.filter(parent__in=menu_items_names).order_by('order')
    return {'menu_items' : menu_items, 'sub_menu_items' : sub_menu_items, 'request' : r}

@register.inclusion_tag('app/carousel.html')
def render_slideshow(r):
    items = SlideShowItem.objects.all().order_by('order')
    return {'items' : items, 'request' : r}

@register.inclusion_tag('app/footer.html')
def render_footer():
    solutions = Solution.objects.all().order_by('order')[:5]
    settings = Settings.objects.get(pk=1)
    return {'solutions' : solutions, 'settings' : settings}

@register.filter
def incr(value):
    return int(value) + 1

@register.filter
def decr(value):
    return int(value) - 1
    
@register.inclusion_tag('app/block_articles.html')
def render_block_articles(r):
    a = Article.objects.all()[:3]
    return {'a' : a, 'request' : r}

@register.inclusion_tag('app/feedback_count.html')
def render_feedback_count():
    count = Feedback.objects.all().count()
    return {'count' : count}    