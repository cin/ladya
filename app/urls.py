#! /usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from . import views
from django.conf.urls import include, url

urlpatterns = [
    url(r'^summernote/', include('django_summernote.urls')),
    url(r'^$', views.index, name='index'),
    url(r'^errors/$', views.errors, name='errors'),
    url(r'^login/$', views.user_login, name='user_login'),
    url(r'^forgot/$', views.user_forgot, name='user_forgot'),
    url(r'^register/$', views.user_register, name='user_register'),
    url(r'^logout/$', views.user_logout, name='user_logout'),
    url(r'^activation/(?P<code>[a-zA-Z_0-9]+)/$', views.user_activation, name='user_activation'),
    url(r'^reset/(?P<code>[a-zA-Z_0-9]+)/$', views.user_reset, name='user_reset'),
    url(r'^profile/$', views.user_view_profile, name='user_view_profile'),
    url(r'^profile/edit/$', views.user_edit_profile, name='user_edit_profile'),

    url(r'^solutions/\b(?P<name>[a-zA-Z]+)\b/$', views.solution_detail, name='solution_detail'),
    url(r'^solutions/$', views.list_solutions, name='list_solutions'),

    url(r'^article/(?P<index>[0-9]+)/$', views.article_detail, name='article_detail'),
    url(r'^articles/$', views.list_articles, name='list_articles'),
    url(r'^articles/(?P<page>[0-9]+)/$', views.list_articles, name='list_articles'),
    
    url(r'^feedback/$', views.feedback, name='feedback'),
    url(r'^message/(?P<index>[0-9]+)/$', views.message, name='message'),
    
    # Администрирование

    url(r'^adm/$', views.admin_panel, name='admin_panel'),

    url(r'^adm/articles/$', views.admin_view_articles, name='admin_view_articles'),
    url(r'^adm/articles/add/$', views.admin_add_article, name='admin_add_article'),
    url(r'^adm/articles/(?P<index>[0-9]+)/$', views.admin_edit_article, name='admin_edit_article'),
    url(r'^adm/articles/delete/(?P<index>[0-9]+)/$', views.admin_delete_article, name='admin_delete_article'),
    
    url(r'^adm/pages/$', views.admin_view_pages, name='admin_view_pages'),
    url(r'^adm/pages/add/$', views.admin_add_page, name='admin_add_page'),
    url(r'^adm/pages/(?P<index>[0-9]+)/$', views.admin_edit_page, name='admin_edit_page'),
    url(r'^adm/pages/delete/(?P<index>[0-9]+)/$', views.admin_delete_page, name='admin_delete_page'),

    url(r'^adm/solutions/$', views.admin_view_solutions, name='admin_view_solutions'),
    url(r'^adm/solutions/add/$', views.admin_add_solution, name='admin_add_solution'),
    url(r'^adm/solutions/(?P<index>[0-9]+)/$', views.admin_edit_solution, name='admin_edit_solution'),
    url(r'^adm/solutions/delete/(?P<index>[0-9]+)/$', views.admin_delete_solution, name='admin_delete_solution'),

    url(r'^adm/users/$', views.admin_view_users, name='admin_view_users'),
    url(r'^adm/users/add/$', views.admin_add_user, name='admin_add_user'),
    url(r'^adm/user/(?P<index>[0-9]+)/$', views.admin_edit_user, name='admin_edit_user'),
    url(r'^adm/users/delete/(?P<index>[0-9]+)/$', views.admin_delete_user, name='admin_delete_user'),

    url(r'^adm/menu/$', views.admin_menu, name='admin_menu'),
    url(r'^adm/menu/add/$', views.admin_add_menu, name='admin_add_menu'),
    url(r'^adm/menu/(?P<index>[0-9]+)/$', views.admin_edit_menu, name='admin_edit_menu'),
    url(r'^adm/menu/delete/(?P<index>[0-9]+)/$', views.admin_delete_menu, name='admin_delete_menu'),

    url(r'^adm/slideshow/$', views.admin_slideshow, name='admin_slideshow'),
    url(r'^adm/slideshow/add/$', views.admin_add_slideshow, name='admin_add_slideshow'),
    url(r'^adm/slideshow/(?P<index>[0-9]+)/$', views.admin_edit_slideshow, name='admin_edit_slideshow'),
    url(r'^adm/slideshow/delete/(?P<index>[0-9]+)/$', views.admin_delete_slideshow, name='admin_delete_slideshow'),
    
    url(r'^adm/feedback/$', views.admin_feedback, name='admin_feedback'),
    url(r'^adm/feedback/(?P<index>[0-9]+)/$', views.admin_view_feedback, name='admin_view_feedback'),
    url(r'^adm/feedback/delete/(?P<index>[0-9]+)/$', views.admin_delete_feedback, name='admin_delete_feedback'),
    
    url(r'^adm/upload/$', views.admin_image_upload, name='admin_image_upload'),
    url(r'^adm/settings/$', views.admin_settings, name='admin_settings'),

    url(r'^shop/$', views.shop, name='shop'),
    url(r'^shop/(?P<category>[0-9]+)/$', views.shop, name='shop'),
    url(r'^shop/(?P<category>[0-9]+)/(?P<page>[0-9]+)/$', views.shop, name='shop'),

    url(r'^google513d82cf66bfe448.html/$', views.google, name='google'),
    url(r'^sitemap.txt/$', views.sitemap, name='sitemap'),
    
    url(r'^(?P<url>.+?)/$', views.page, name='views.page'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)