#! /usr/bin/env python
# -*- coding: utf-8 -*-
from django.shortcuts import render, get_object_or_404, render_to_response
from django.utils import timezone
from django.shortcuts import redirect
from django.contrib.auth.hashers import make_password, check_password
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.core.urlresolvers import reverse
from django.core.files import storage
from django.core.files.storage import FileSystemStorage
from django.conf import settings
from os import walk
import PIL
from PIL import Image
from .models import Solution
from .models import Article
from .models import UserProfile
from .models import MenuItem
from .models import SlideShowItem
from .models import Page
from .models import Feedback
from .models import Settings

from .forms import SolutionForm
from .forms import ArticleForm
from .forms import MenuForm
from .forms import PageForm
import string
import random
import glob, os, sys, logging
import os.path
import json
from os.path import splitext
from app.templatetags import mytags
from datetime import datetime, timedelta
from django.core.mail import send_mail

def id_generator(size=10, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

# Главная страница

def index(request):
    solutions = Solution.objects.all().order_by('order')
    return render(request, 'app/index.html', {'solutions' : solutions})

# Решения

def solution_detail(request, name):
    solution = get_object_or_404(Solution, name=name)
   
    return render(request, 'app/detail_solution.html', {'solution' : solution})

def list_solutions(request):
    solutions = Solution.objects.all().order_by('order')
    return render(request, 'app/list_solutions.html', {'solutions' : solutions})

# ошибка

def errors(request, errors):
    return render(request, 'app/errors.html', {'errors' : errors})

# Статьи

def article_detail(request, index):
    next_article = None
    try:
        for i in range(int(index)+1,int(index)+6):
            next_article = Article.objects.get(pk=i)
            if next_article != None:
                if next_article.published:
                    break
    except:
        pass

    article = get_object_or_404(Article, pk=index)
    if article.published:
        return render(request, 'app/detail_article.html', {'article' : article, 'next_article' : next_article})
    return redirect('list_articles')

def list_articles(request, page = 1):
    q = Article.objects.all().filter(published=True)
    pg = int(page)
    articles = q.order_by('created_date').filter(published=True)[(pg-1)*10 : (pg-1)*10+10]
    cnt = q.count()
    pages_count = cnt//10.0
    frac = cnt % 10
    if cnt > 10 and frac > 0:
        pages_count = pages_count + 1
    return render(request, 'app/list_articles.html', {'articles' : articles, 'pages_count' : int(pages_count), 'page' : int(page)})

# Админка

def admin_panel(request):
    if not request.user.is_authenticated() or not request.user.is_staff:
        return redirect('index')

    solutions = Solution.objects.filter(created_date__lte=timezone.now()).order_by('order')[:5]
    articles = Article.objects.all().order_by('created_date')[:5]
    return render(request, 'app/admin_panel_main.html', {'solutions' : solutions, 'articles' : articles})

# Решения

def admin_view_solutions(request):
    if not request.user.is_authenticated() or not request.user.is_staff:
        return redirect('index')

    solutions = Solution.objects.all().order_by('order')

    return render(request, 'app/admin_view_solutions.html', {'solutions' : solutions})

def admin_add_solution(request):
    if not request.user.is_authenticated() or not request.user.is_staff:
        return redirect('index')

    files = []
    for (dirpath, dirnames, filenames) in walk(settings.MEDIA_ROOT+"/images/"):
        files.extend(filenames)
        break

    if request.method == "POST":
        form = SolutionForm(request.POST)
        if form.is_valid():
            solution = form.save(commit=False)
            solution.author = request.user
            solution.created_date = timezone.now()
            
            if solution.desc_short_image == "picked":
                if request.FILES.get('upload_sd_image', False):
                    f = request.FILES['upload_sd_image']
                    with open(settings.MEDIA_ROOT+"/images/"+f.name, 'wb+') as destination:
                        for chunk in f.chunks():
                            destination.write(chunk)
                        destination.close()
                    fname = settings.MEDIA_ROOT+"/images/"+f.nam
                    img = Image.open(fname)
                    image = img.resize((140, 140), Image.ANTIALIAS)
                    image.save(fname, quality=90)
                    solution.desc_short_image = fname
                
            if solution.grid_image == "picked":
                if request.FILES.get('upload_cell_image', False):
                    f = request.FILES['upload_cell_image']
                    with open(settings.MEDIA_ROOT+"/images/"+f.name, 'wb+') as destination:
                        for chunk in f.chunks():
                            destination.write(chunk)
                        destination.close()
                    fname = settings.MEDIA_ROOT+"/images/"+f.nam
                    img = Image.open(fname)
                    image = img.resize((700, 467), Image.ANTIALIAS)
                    image.save(fname, quality=90)
                    solution.grid_image = fname
            
            solution.save()
            return redirect('admin_view_solutions')
        else:
            err = form.errors
            return render('errors', {'error':form.errors})
    else:
        form = SolutionForm()

    return render(request, 'app/admin_add_solution.html', {'form' : form, 'files' : files})

def admin_edit_solution(request, index):
    if not request.user.is_authenticated() or not request.user.is_staff:
        return redirect('index')

    solution = Solution.objects.get(pk=index)

    files = []
    for (dirpath, dirnames, filenames) in walk(settings.MEDIA_ROOT+"/images/"):
        files.extend(filenames)
        break

    if request.method == "POST":
        form = SolutionForm(request.POST, instance=solution)
        if form.is_valid():
            solution = form.save(commit=False)
            solution.author = request.user
            solution.created_date = timezone.now()
            solution.save()
            return redirect('admin_view_solutions')
        else:
            err = form.errors
            return render('errors', {'error':form.errors})
    else:
        form = SolutionForm(instance=solution)

    return render(request, 'app/admin_edit_solution.html', {'form' : form, 'solution' : solution, 'files' : files})

def admin_delete_solution(request, index):
    if not request.user.is_authenticated() or not request.user.is_staff:
        return redirect('index')

    item = Solution.objects.get(pk=index)
    item.delete()
    return redirect('admin_view_solutions')

# Меню

def admin_menu(request):
    if not request.user.is_authenticated() or not request.user.is_staff:
        return redirect('index')

    items = MenuItem.objects.all()
    return render(request, 'app/admin_menu.html', {'items' : items})

def admin_add_menu(request):
    if not request.user.is_authenticated() or not request.user.is_staff:
        return redirect('index')

    if request.method == "POST":
        form = MenuForm(request.POST)

        if form.is_valid():
            item = form.save(commit=False)
            item.save()
            return redirect('admin_menu')
        else:
            err = form.errors
            return render('errors', {})
    else:
        form = MenuForm()

    return render(request, 'app/admin_add_menu.html', {'form' : form})

def admin_edit_menu(request, index):
    if not request.user.is_authenticated() or not request.user.is_staff:
        return redirect('index')

    item = MenuItem.objects.get(pk=index)
    form = MenuForm(request.POST, instance=item)

    if request.method == "POST":
        if form.is_valid():
            item = form.save(commit=False)
            item.save()
            return redirect('admin_menu')
        else:
            err = form.errors
            return render('errors', {})

    return render(request, 'app/admin_edit_menu.html', {'item' : item})

def admin_delete_menu(request, index):
    if not request.user.is_authenticated() or not request.user.is_staff:
        return redirect('index')

    item = MenuItem.objects.get(pk=index)
    item.delete()
    return redirect('admin_menu')

# Загрузка изображений

def admin_image_upload(request):
    if not request.user.is_authenticated():
        return index(request)
    if (not request.user.is_staff):
        return index(request)

    if request.method == 'POST':
        fl = ""
        f = request.FILES['upload_image']
        if (f.size > 2621440):
            with open(settings.MEDIA_ROOT+"/images/"+f.name, 'wb+') as destination:
                for chunk in f.chunks():
                    destination.write(chunk)
                destination.close()
            fl = f.name
        else:
            storage = FileSystemStorage(location = settings.MEDIA_ROOT+"/images", base_url = '/images')
            name = storage.save(None, f)
            fl = storage.url(name)

        return render(request, 'app/admin_upload.html', {'file':fl, 'size' : f.size})

    return render(request, 'app/admin_upload.html', {})

# Пользователи

def admin_view_users(request):
    if not request.user.is_authenticated() or not request.user.is_staff:
        return redirect('index')

    profiles = UserProfile.objects.all()
    return render(request, 'app/admin_view_users.html', {'profiles' : profiles})

def admin_add_user(request):
    if not request.user.is_authenticated() or not request.user.is_staff:
        return redirect('index')

    error = 0
    if request.method == 'POST':
        username    = request.POST['username']
        email       = request.POST['email']
        password    = request.POST['password']
        firstname   = request.POST['first_name']
        secondname  = request.POST['second_name']
        patronymic  = request.POST['patronymic']
        company     = request.POST['company']
        address     = request.POST['address']
        phone       = request.POST['phone']
        role        = request.POST['role']
        activated = "off"
        if request.POST.get('activated', False):
            activated = request.POST['activated']
        
        if username == "" or password == "" or email == "" or firstname == "" or secondname == "" or patronymic == "" or company == "" or address == "" or phone == "":
            error = 1
            return render(request, 'app/admin_add_user.html', {'error' : error})
        
        user = User.objects.create_user(username, email, password)
        user.first_name = firstname
        user.last_name = secondname
        user.is_active = (activated == "on")
        user.is_staff = role
        profile = UserProfile()
        profile.user = user
        profile.patronymic = patronymic
        profile.company = company
        profile.address = address
        profile.phone = phone
        profile.solutions = ""
        profile.activation = ""
        user.save()
        profile.save()

        if request.FILES.get('upload_image', False):
            f = request.FILES['upload_image']
            ava = id_generator(10) + f.name

            with open(settings.MEDIA_ROOT+"/users/"+ava, 'wb+') as destination:
                for chunk in f.chunks():
                    destination.write(chunk)
                destination.close()

            img = Image.open(settings.MEDIA_ROOT+"/users/"+ava)
            image = img.resize((200, 200), Image.ANTIALIAS)
            filename, ext = os.path.splitext(ava)
            thumb = image.resize((50, 50), Image.ANTIALIAS)
            thumb.save(settings.MEDIA_ROOT+"/users/"+filename + "_thumb"+ext, quality=90)
            image.save(settings.MEDIA_ROOT+"/users/"+ava, quality=90)
            profile.avatar = ava
            profile.save()

        return redirect("admin_view_users")

    return render(request, 'app/admin_add_user.html', {})

def admin_edit_user(request, index):
    if not request.user.is_authenticated() or not request.user.is_staff:
        return redirect('index')

    if not UserProfile.objects.filter(pk=index).exists():
        return redirect('index')
    
    profile = UserProfile.objects.get(pk=index)

    error = 0
    if request.method == 'POST':

        username    = request.POST['username']
        email       = request.POST['email']
        password    = request.POST['password']
        firstname   = request.POST['first_name']
        secondname  = request.POST['second_name']
        patronymic  = request.POST['patronymic']
        company     = request.POST['company']
        address     = request.POST['address']
        phone       = request.POST['phone']
        activated = "off"
        if request.POST.get('activated', False):
            activated   = request.POST['activated']
        
        if username == "" or email == "" or firstname == "" or secondname == "" or patronymic == "" or company == "" or address == "" or phone == "":
            error = 2
            return render(request, 'app/user_edit_profile.html', {'solutions' : solutions,'error' : error})
        
        user = profile.user
        if password != "":
            psw = make_password(password)
            user.password = psw


        user.username = username
        user.email = email
        user.first_name = firstname
        user.last_name = secondname
        user.is_active = (activated == "on")
        user.save()
        error = 1
        profile = user.userprofile
        profile.patronymic = patronymic;
        profile.company = company;
        profile.address = address;
        profile.phone = phone;
        #profile.solutions = "";
        profile.save()

        if request.FILES.get('upload_image', False):
            f = request.FILES['upload_image']
            ava = id_generator(10) + f.name

            path = settings.MEDIA_ROOT+"/users/"
            if not os.path.exists(path):
                os.makedirs(path)
            with open(path+ava, 'wb+') as destination:
                for chunk in f.chunks():
                    destination.write(chunk)
                destination.close()

            img = Image.open(path+ava)
            image = img.resize((200, 200), Image.ANTIALIAS)
            filename, ext = os.path.splitext(ava)
            thumb = image.resize((50, 50), Image.ANTIALIAS)
            thumb.save(path+filename + "_thumb"+ext, quality=90)
            image.save(path+ava, quality=90)
            profile.avatar = ava
            profile.save()

    return render(request, 'app/admin_edit_user.html', {'profile' : profile})

def admin_delete_user(request, index):
    if not request.user.is_authenticated() or not request.user.is_staff:
        return redirect('index')
    profile = UserProfile.objects.get(pk=index)
    if profile.user.is_superuser == 1:
        return redirect('admin_view_users')
    profile.user.delete()
    profile.delete()
    return redirect('admin_view_users')

# Слайдшоу

def admin_slideshow(request):
    if not request.user.is_authenticated() or not request.user.is_staff:
        return redirect('index')

    items = SlideShowItem.objects.all().order_by('order')
    return render(request, 'app/admin_slideshow.html', {'items' : items})

def admin_add_slideshow(request):
    if not request.user.is_authenticated() or not request.user.is_staff:
        return redirect('index')

    files = []
    for (dirpath, dirnames, filenames) in walk(settings.MEDIA_ROOT+"/images/"):
        files.extend(filenames)
        break

    if request.method == "POST":
        order       = request.POST['order']
        title       = request.POST['title']
        description = request.POST['description']
        button_text = request.POST['button_text']
        button_url  = request.POST['button_url']
        slide_image = request.POST['slide_image']
        
        if description == "":
            error = 1
            return render(request, 'app/admin_add_slideshow.html', {'error' : error, 'files' : files})

        item = SlideShowItem()
        item.order = order
        item.title = title
        item.background  = "default_slideshow.jpg"
        item.description = description
        item.button_text = button_text
        item.button_url  = button_url

        if slide_image == "picked":
            if request.FILES.get('upload_image', False):
                f = request.FILES['upload_image']
                with open(settings.MEDIA_ROOT+"/images/"+f.name, 'wb+') as destination:
                    for chunk in f.chunks():
                        destination.write(chunk)
                    destination.close()
                img = Image.open(settings.MEDIA_ROOT+"/images/"+f.name)
                image = img.resize((1920, 600), Image.ANTIALIAS)
                image.save(settings.MEDIA_ROOT+"/images/"+f.name, quality=90)
                item.background = f.name
        else:
            item.background = slide_image
        
        item.save()
        return redirect('admin_slideshow')

    return render(request, 'app/admin_add_slideshow.html', {'files' : files})

def admin_edit_slideshow(request, index):
    if not request.user.is_authenticated() or not request.user.is_staff:
        return redirect('index')
    
    item = SlideShowItem.objects.get(pk=index)

    files = []
    for (dirpath, dirnames, filenames) in walk(settings.MEDIA_ROOT+"/images/"):
        files.extend(filenames)
        break

    if request.method == "POST":
        order       = request.POST['order']
        title       = request.POST['title']
        description = request.POST['description']
        button_text = request.POST['button_text']
        button_url  = request.POST['button_url']
        slide_image = request.POST['slide_image']
        
        if description == "":
            error = 1
            return render(request, 'app/admin_edit_slideshow.html', {'error' : error, 'files' : files, 'item':item})

        item.order = order
        item.title = title
        item.background  = "default_slideshow.jpg"
        item.description = description
        item.button_text = button_text
        item.button_url  = button_url

        if slide_image == "picked":
            if request.FILES.get('upload_image', False):
                f = request.FILES['upload_image']
                with open(settings.MEDIA_ROOT+"/images/"+f.name, 'wb+') as destination:
                    for chunk in f.chunks():
                        destination.write(chunk)
                    destination.close()
                img = Image.open(settings.MEDIA_ROOT+"/images/"+f.name)
                image = img.resize((1920, 600), Image.ANTIALIAS)
                image.save(settings.MEDIA_ROOT+"/images/"+f.name, quality=90)
                item.background = f.name
        else:
            item.background = slide_image
        
        item.save()
        return redirect('admin_slideshow')

    return render(request, 'app/admin_edit_slideshow.html', {'files' : files, 'item':item})

def admin_delete_slideshow(request, index):
    if not request.user.is_authenticated() or not request.user.is_staff:
        return redirect('index')

    item = SlideShowItem.objects.get(pk=index)
    item.delete()
    return redirect('admin_slideshow')

# Статьи

def admin_view_articles(request):
    if not request.user.is_authenticated() or not request.user.is_staff:
        return redirect('index')

    articles = Article.objects.all().order_by('pk')
    return render(request, 'app/admin_view_articles.html', {'articles' : articles})

def admin_add_article(request):
    if not request.user.is_authenticated() or not request.user.is_staff:
        return redirect('index')

    if request.method == "POST":
        form = ArticleForm(request.POST)
        if form.is_valid():
            article = form.save(commit=False)
            article.author = request.user
            article.created_date = timezone.now()
            article.save()
            return redirect('admin_view_articles')
        else:
            err = form.errors
            return render('errors', {'error':form.errors})
    else:
        form = ArticleForm()

    return render(request, 'app/admin_add_article.html', {'form' : form})

def admin_edit_article(request, index):
    if not request.user.is_authenticated() or not request.user.is_staff:
        return redirect('index')

    article = Article.objects.get(pk=index)
    
    if request.method == "POST":
        form = ArticleForm(request.POST, instance=article)
        if form.is_valid():
            article = form.save(commit=False)
            article.author = request.user
            article.save()
            return redirect('admin_view_articles')
        else:
            err = form.errors
            return render('errors', {'error':form.errors})
    else:
        form = ArticleForm(instance = article)

    return render(request, 'app/admin_edit_article.html', {'form' : form, 'article' : article})

def admin_delete_article(request, index):
    if not request.user.is_authenticated() or not request.user.is_staff:
        return redirect('index')

    item = Article.objects.get(pk=index)
    item.delete()
    return redirect('admin_view_articles')


# Страницы

def admin_view_pages(request):
    if not request.user.is_authenticated() or not request.user.is_staff:
        return redirect('index')

    pages = Page.objects.all().order_by('pk')
    return render(request, 'app/admin_view_pages.html', {'pages' : pages})

def admin_add_page(request):
    if not request.user.is_authenticated() or not request.user.is_staff:
        return redirect('index')

    if request.method == "POST":
        form = PageForm(request.POST)
        if form.is_valid():
            page = form.save(commit=False)
            page.save()
            return redirect('admin_view_pages')
        else:
            err = form.errors
            return render('errors', {'error':form.errors})
    else:
        form = PageForm()

    return render(request, 'app/admin_add_page.html', {'form' : form})

def admin_edit_page(request, index):
    if not request.user.is_authenticated() or not request.user.is_staff:
        return redirect('index')

    page = Page.objects.get(pk=index)
    
    if request.method == "POST":
        form = PageForm(request.POST, instance=page)
        if form.is_valid():
            page.save()
            return redirect('admin_view_pages')
        else:
            err = form.errors
            return render('errors', {'error':form.errors})
    else:
        form = PageForm(instance = page)

    return render(request, 'app/admin_edit_page.html', {'form' : form, 'page' : page})

def admin_delete_page(request, index):
    if not request.user.is_authenticated() or not request.user.is_staff:
        return redirect('index')

    item = Page.objects.get(pk=index)
    item.delete()
    return redirect('admin_view_pages')



def admin_feedback(request):
    if not request.user.is_authenticated() or not request.user.is_staff:
        return redirect('index')
           
    items = Feedback.objects.all().order_by('pk')
    return render(request, 'app/admin_feedback.html', {'feedbacks' : items})    
    
def admin_view_feedback(request, index):
    if not request.user.is_authenticated() or not request.user.is_staff:
        return redirect('index')

    if request.method == 'POST':
        email = request.POST['email']
        text = request.POST['text']
        settings = Settings.objects.get(pk=1)
        mcount = send_mail(u'Ладья Телематика. Ответ на ваше обращение', text, 'support@vostokgps.ru', [email], fail_silently=True)

        if mcount == 0:
            return JsonResponse({'message': "Возникла ошибка при отправки письма", 'status': 2})

        return JsonResponse({'message': "Сообщение отправлено: "+str(mcount), 'status': 1})
        
    item = Feedback.objects.get(pk=index)
    return render(request, 'app/admin_view_feedback.html', {'feedback' : item})

def admin_delete_feedback(request, index):
    if not request.user.is_authenticated() or not request.user.is_staff:
        return redirect('index')

    item = Feedback.objects.get(pk=index)
    item.delete()
    return redirect('admin_feedback')

def admin_settings(request):
    settings = Settings.objects.get(pk=1)

    if request.method == 'POST':
        try:
            site_active = request.POST['site_active']
            site_message = request.POST['site_message']
            address = request.POST['address']
            phone = request.POST['phone']
            email = request.POST['email']
            settings.site_active = site_active
            settings.site_message = site_message
            settings.address = address
            settings.phone = phone
            settings.email = email
            settings.save()
        except Exception as ex:
            return JsonResponse({'message': ex.message, 'status': 2})
        return JsonResponse({'message': "Настройки успешно сохранены", 'status': 1})

    return render(request, 'app/admin_settings.html', {'settings' : settings})

# Пользователь

def user_login(request):
    if request.user.is_authenticated():
        return index(request)

    error = 0
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None and user.is_active:
            login(request, user)
            if user.is_staff:
                return redirect('admin_panel')
            return redirect('index')
        else:
            error = 1

    solutions = Solution.objects.filter(created_date__lte=timezone.now()).order_by('order')
    return render(request, 'app/user_login.html', {'solutions' : solutions,'error' : error})

def user_logout(request):
    if request.user.is_authenticated():
        logout(request)

    return redirect('index')

def user_register(request):
    if request.user.is_authenticated():
        return redirect('index')

    error = 0
    if request.method == 'POST':
        username    = request.POST['username']
        email       = request.POST['email']
        password    = request.POST['password']
        firstname   = request.POST['first_name']
        secondname  = request.POST['second_name']
        patronymic  = request.POST['patronymic']
        company     = request.POST['company']
        address     = request.POST['address']
        phone       = request.POST['phone']

        if username == "" or email == "" or password == "" or firstname == "" or secondname == "" or patronymic == "" or company == "" or address == "" or phone == "":
            error = 1
            return render(request, 'app/user_register.html', {'error' : error})

        if User.objects.filter(email__exact=email).exists():
            error = 3
            return render(request, 'app/user_register.html', {'error' : error})

        if User.objects.filter(username__exact=username).exists():
            error = 2
            return render(request, 'app/user_register.html', {'error' : error})

        user = User.objects.create_user(username, email, password)
        user.first_name = firstname
        user.last_name = secondname
        user.is_active = False
        profile = UserProfile()
        profile.user = user
        profile.patronymic = patronymic
        profile.company = company
        profile.address = address
        profile.phone = phone
        profile.solutions = ""
        profile.activation = id_generator(10)
        user.save()
        profile.save()
        user.email_user(u'Ладья Телематика: Активация аккаунта', u'Здравствуйте. Ссылка для активации аккаунта: http://vostokgps.ru/activation/'+profile.activation, from_email=None)
        error = 3
        return redirect('/message/3/')

    return render(request, 'app/user_register.html', {'error' : error})

def user_forgot(request):
    if request.user.is_authenticated():
        return redirect('index')

    error = 0

    solutions = Solution.objects.filter(created_date__lte=timezone.now()).order_by('order')

    if request.method == 'POST':
        e_mail = request.POST['email']

        if not User.objects.filter(email__exact=e_mail).exists():
            error = 1
            return render(request, 'app/user_forgot.html', {'solutions' : solutions,'error' : error})

        usr = User.objects.get(email__exact=e_mail)
        if not usr.is_active:
            error = 2
            return render(request, 'app/user_forgot.html', {'solutions' : solutions,'error' : error})
        
        userprofile = UserProfile.objects.get(user=usr)
        userprofile.activation = id_generator(10)
        userprofile.save()
        usr.email_user(u'Ладья телематика: Восстановление пароля', u'Здравствуйте. Ссылка для получения нового пароля: http://vostokgps.ru/reset/'+userprofile.activation, from_email=None)
        error = 3
        return redirect('/message/1/')
    
    return render(request, 'app/user_forgot.html', {'solutions' : solutions,'error' : error})

def user_activation(request, code):
    solutions = Solution.objects.filter(created_date__lte=timezone.now()).order_by('order')

    if UserProfile.objects.filter(activation__exact=code).exists():
        profile = UserProfile.objects.get(activation=code)
        user = profile.user
        if user.is_active == False:
            user.is_active = True
            profile.activation = ""
            profile.save()
            user.save()

    return redirect('user_login')

def user_view_profile(request):
    if not request.user.is_authenticated():
        return index(request)

    solutions = Solution.objects.filter(created_date__lte=timezone.now()).order_by('order')

    return render(request, 'app/user_view_profile.html', {'solutions' : solutions})

def user_edit_profile(request):
    if not request.user.is_authenticated():
        return redirect('index')

    solutions = Solution.objects.filter(created_date__lte=timezone.now()).order_by('order')

    error = 0
    if request.method == 'POST':
        email       = request.POST['email']
        old_pass    = request.POST['old_pass']
        password    = request.POST['password']
        firstname   = request.POST['first_name']
        secondname  = request.POST['second_name']
        patronymic  = request.POST['patronymic']
        company     = request.POST['company']
        address     = request.POST['address']
        phone       = request.POST['phone']
        
        if email == "" or old_pass == "" or firstname == "" or secondname == "" or patronymic == "" or company == "" or address == "" or phone == "":
            error = 2
            return render(request, 'app/user_edit_profile.html', {'solutions' : solutions,'error' : error})

        if check_password(old_pass, request.user.password) == False:
            error = 3
            return render(request, 'app/user_edit_profile.html', {'solutions' : solutions,'error' : error})

        if password != "":
            psw = make_password(password)

        user = request.user
        user.last_name = secondname

        if password != "":
            user.password = psw

        user.save()
        error = 1
        profile = user.userprofile
        profile.patronymic = patronymic;
        profile.company = company;
        profile.address = address;
        profile.phone = phone;
        #profile.solutions = "";
        profile.save()

        if request.FILES.get('upload_image', False):
            f = request.FILES['upload_image']
            ava = id_generator(10) + f.name

            with open(settings.MEDIA_ROOT+"/users/"+ava, 'wb+') as destination:
                for chunk in f.chunks():
                    destination.write(chunk)
                destination.close()
            img = Image.open(settings.MEDIA_ROOT+"/users/"+ava)
            image = img.resize((200, 200), Image.ANTIALIAS)
            image.save(settings.MEDIA_ROOT+"/users/"+ava, quality=90)
            filename, ext = os.path.splitext(ava)
            thumb = image.resize((50, 50), Image.ANTIALIAS)
            thumb.save(settings.MEDIA_ROOT+"/users/"+filename + "_thumb"+ext, quality=90)
            profile.avatar = ava
            profile.save()

        return render(request, 'app/user_edit_profile.html', {'solutions' : solutions,'error' : error})

    return render(request, 'app/user_edit_profile.html', {'solutions' : solutions,'error' : error})

def user_reset(request, code):
    if UserProfile.objects.filter(activation__exact=code).exists():
        profile = UserProfile.objects.get(activation=code)
        user = profile.user
        password = User.objects.make_random_password()
        user.email_user(u'Ладья Телематика. Восстановление пароля', u'Здравствуйте. Ваш новый пароль: ' + password, from_email=None)
        user.set_password(password)
        profile.activation = ""
        profile.save()
        user.save()

    return redirect('/message/1/')


def page(request, url):
    page = get_object_or_404(Page, url = url)
    return render(request, 'app/page.html', {'page' : page})

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def feedback(request):
    if request.method == 'POST':
        ip = get_client_ip(request)
               
        firstname = request.POST['firstname']
        email = request.POST['email']
        topic = request.POST['topic']
        text = request.POST['text']
        
        feedback = Feedback()
        feedback.author = firstname
        feedback.email = email
        feedback.usr = User.objects.get(pk=1)
        if request.user.is_authenticated():
            feedback.usr = request.user

        feedback.topic = topic
        feedback.text = text
        feedback.ip = ip
        feedback.created_date = timezone.now()

        timediff = timedelta()
        fb = None
        try:
            fb = Feedback.objects.filter(ip=ip).order_by('-pk')[0]
        except:
            pass

        if fb != None:
            now = datetime.now(timezone.utc)
            timediff = now - fb.created_date
            ts = timediff.total_seconds()
            if ts < 300:
                mm =int((300-ts)/60)
                st = str(mm)+" мин."
                if (mm == 0):
                    st = "менее минуты."
                return JsonResponse({'message': "Сообщение можно отправить каждые 5 минут. Подождите "+st, 'status': 2})
        feedback.save()
        email_text = u'Новое обращение на сайте.\r\nАвтор: '+firstname+u'\r\nEmail: '+email+u'\r\nТема: '+topic+u'\r\nТекст: '+text+u'\r\n\r\nОтветить: http://vostokgps.ru/adm/feedback/'
        try:
            send_mail(u'Новое обращение на сайте', email_text, 'support@vostokgps.ru', ['support@vostokgps.ru'], fail_silently=True)
        except:
            pass

        return JsonResponse({'message': "Ваше сообщение отправлено. Спасибо.", 'status': 1})
    
    feedback = Feedback()
    feedback.usr = User.objects.get(pk=1)
    if request.user.is_authenticated():
        feedback.usr = request.user
        feedback.author = request.user.first_name + " " + request.user.last_name
        feedback.email = request.user.email

    return render(request, 'app/feedback.html', {'feedback' : feedback})
    
def message(request, index = 0):
    index = int(index)
    title = "Заголовок"
    text = "Текст"
    if index == 1:
        title = "Восстановление пароля"
        text = "Новый пароль выслан на указанный адрес электронной почты."

    if index == 2:
        title = "Восстановление пароля"
        text = "Ссылка для восстановления пароля вылана на указанный адрес электронной почты"


    if index == 3:
        title = "Активация аккаунта"
        text = "Ссылка для активации аккаунта вылана на указанный адрес электронной почты"

    return render(request, 'app/message.html', {'title' : title, 'text' : text})


def shop(request, page=0, category=0):
    if not request.user.is_authenticated() or not request.user.is_staff:
        return redirect('index')
    return render(request, "app/shop_main.html", {})

def google(request):
    return render(request, "app/google513d82cf66bfe448.html", {})

def sitemap(request):
    return render(request, "app/sitemap.txt", {})