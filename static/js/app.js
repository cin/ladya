$('#form-feedback').submit(submit_feedback)
$('#feedback_answer').submit(submit_feedback_answer)
$('#settings_form').submit(submit_settings)


function submit_feedback(e) {
    $.ajax({
        data: $(this).serialize(),
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        dataType: 'json',
        success: function(response) {
            if (response.status == 1) {
                $("#alert").html(response.message);
                $("#alert").removeClass("alert-hidden");
                $("#alert-red").addClass("alert-hidden");
                $("#topic").val("");
                $("#text").val("");
            } else
            if (response.status == 2) {
                $("#alert-red").html(response.message);
                $("#alert-red").removeClass("alert-hidden");
                $("#alert").addClass("alert-hidden");
            }
        },
        error: function(e, x, r) {
            $("#alert-red").html(r.message);
            $("#alert-red").removeClass("alert-hidden");
        }
    });
    e.preventDefault();
    return false;
}

function submit_feedback_answer(e) {
    $.ajax({
        data: $(this).serialize(),
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        dataType: 'json',
        success: function(response) {
            if (response.status == 1) {
                $("#alert").html(response.message);
                $("#alert").removeClass("alert-hidden");
                $("#alert-red").addClass("alert-hidden");
                $("#text").val("");
            } else
            if (response.status == 2) {
                $("#alert-red").html(response.message);
                $("#alert-red").removeClass("alert-hidden");
                $("#alert").addClass("alert-hidden");
            }
        },
        error: function(e, x, r) {
            $("#alert-red").html(r.message);
            $("#alert-red").removeClass("alert-hidden");
        }
    });
    e.preventDefault();
    return false;
}

function submit_settings(e) {
    $.ajax({
        data: $(this).serialize(),
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        dataType: 'json',

        success: function(response) {
            if (response.status == 1) {
                $("#alert").html(response.message);
                $("#alert").removeClass("alert-hidden");
                $("#alert-red").addClass("alert-hidden");
            } else
            if (response.status == 2) {
                $("#alert-red").html(response.message);
                $("#alert-red").removeClass("alert-hidden");
                $("#alert").addClass("alert-hidden");
            }
        },
        error: function(e, x, r) {
            $("#alert-red").html(r.message);
            $("#alert-red").removeClass("alert-hidden");
            $("#alert").addClass("alert-hidden");
        }
    });
    e.preventDefault();
    return false;
}